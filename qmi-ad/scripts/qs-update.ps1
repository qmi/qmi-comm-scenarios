<#
Module:             qs-update.ps1
Author:             Clint Carr
Modified by:        -
Modification History:
 - Added Logging
 - Added comments
 - Set up error checking
last updated:       10/11/2017
Intent: Perform patch updates
#>
Write-Log -Message "Starting qs-update.ps1"
If (Test-Path "c:\shared-content\binaries\Qlik_Sense_update.exe")
        {
            Write-Log -Message "Installing Update"
            Unblock-File -Path c:\shared-content\binaries\Qlik_Sense_Update.exe
            Invoke-Command -ScriptBlock {Start-Process -FilePath "c:\shared-content\binaries\Qlik_Sense_Update.exe" -ArgumentList "install" -Wait -Passthru } | Out-Null
            Get-Service Qlik* | where {$_.Name -ne 'QlikLoggingService'} | Start-Service | Out-Null
            Get-Service Qlik* | where {$_.Name -eq 'QlikSenseServiceDispatcher'} | Stop-Service | Out-Null
            Get-Service Qlik* | where {$_.Name -eq 'QlikSenseServiceDispatcher'} | Start-Service | Out-Null
            $statusCode = 0
            while ($StatusCode -ne 200)
            {
                write-Log -Message "StatusCode is $StatusCode"
                try { $statusCode = (invoke-webrequest https://$([System.Net.DNS]::GetHostByName($env:ComputerName).HostName)/qps/user -usebasicParsing).statusCode }
                Catch
                    {
                        write-Log -Message "Server down, waiting 20 seconds"
                        start-Sleep -s 20
                    }
            }
        If ( Get-Service QlikEAPowerToolsServiceDispatcher -ErrorAction SilentlyContinue ) {
            Write-log -Message "Restarting QlikEAPowerToolsServiceDispatcher..."
            Restart-Service QlikEAPowerToolsServiceDispatcher | Out-Null
         }
        }