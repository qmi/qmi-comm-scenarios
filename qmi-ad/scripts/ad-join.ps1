<#
Module:             ad-join
Author:             Clint Carr
Modified by:        -
Modification History:
 - Added Logging
 - Added comments
 - Set up error checking
last updated:       10/11/2017
Intent: Script to join hosts to QlikLocal.net Domain
#>
Write-Log -Message "Acquiring Network Interface for local network"
$networkIntId = Get-NetIPInterface | ? {$_.InterfaceAlias -eq "Ethernet 2" -and $_.AddressFamily -eq "IPv4" } | select ifIndex

Write-Log -Message "Configuring DNS for local network"
Set-DnsClientServerAddress -interfaceIndex $networkIntId.ifIndex -ServerAddresses ("192.168.56.3") | Out-Null

Write-Log -Message "Adding machine to QlikLocal.net domain"
$credential = New-Object System.Management.Automation.PSCredential("qliklocal\vagrant", (ConvertTo-SecureString "vagrant" -AsPlainText -Force))
Add-Computer -DomainName "qliklocal.net" -Credential $credential  | Out-Null

Write-Log -Message "Setting Vagrant user to autologon"
$registryPath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
New-ItemProperty -Path $registryPath -Name DefaultUserName -value vagrant -PropertyType String -Force | Out-Null
New-ItemProperty -Path $registryPath -Name DefaultPassword -value vagrant -PropertyType String -Force | Out-Null
New-ItemProperty -Path $registryPath -Name AutoAdminLogon -value 1 -PropertyType String -Force | Out-Null

