<#
Module:             qs-post-cfg.ps1
Author:             Clint Carr
Modified by:        -
Modification History:
 - Added Logging
 - Added comments
 - Set up error checking
last updated:       10/11/2017
Intent: Post config before installation of Qlik Sense
#>
Write-Log "Starting qs-post-cfg.ps1"
$license = (Get-Content c:\shared-content\licenses\qlik-license.json -raw) | ConvertFrom-Json
$config = (Get-Content c:\vagrant\files\qs-cfg.json -raw) | ConvertFrom-Json

$statusCode = 0
while ($StatusCode -ne 200)
{
    Write-Log -Message "StatusCode is $StatusCode"
    try { $statusCode = (invoke-webrequest https://$([System.Net.DNS]::GetHostByName($env:ComputerName).HostName)/qps/user -usebasicParsing).statusCode }
    Catch
        {
            Write-Log -Message "Server down, waiting 20 seconds"
            start-Sleep -s 20
        }
}
Write-Log -Message "Connecting to Qlik Sense Repository Service"
Connect-Qlik $([System.Net.DNS]::GetHostByName($env:ComputerName).HostName) | Out-Null

Write-Log -Message "Setting license $license.sense.serial"
Set-QlikLicense -serial $license.sense.serial -control $license.sense.control -name "$($license.sense.name)" -organization "$($license.sense.organization)" -lef "$($license.sense.lef)" | Out-Null

Write-Log -Message "Creating user access rule"
$userAccessGroup = (@{name = "License Everyone";} | ConvertTo-Json -Compress -Depth 10)
$licenseId = Invoke-QlikPost "/qrs/License/UserAccessGroup" $userAccessGroup
$systemRuleJson = (@{
    name = "Grant Everyone a token";
    category = "License";
    rule = '((user.name like "*"))';
    type = "Custom";
    resourceFilter = "License.UserAccessGroup_" + $licenseId.id;
    actions = 1;
    ruleContext = "QlikSenseOnly";
    disabled = $false;
    comment = "Rule to set up automatic user access";} | ConvertTo-Json -Compress -Depth 10)
Invoke-QlikPost "/qrs/SystemRule" $systemRuleJson | Out-Null

Write-Log -Message "Adding qlik user to user directory QLIKLOCAL"
$json = (@{userId = "qlik";
            userDirectory = "QLIKLOCAL";
            name = "qlik";
        } | ConvertTo-Json -Compress -Depth 10 )
Invoke-QlikPost "/qrs/user" $json | Out-Null

Write-Log -Message "Granting RootAdmin role to qlik user"
Update-QlikUser -id $(Get-QlikUser -full -filter "name eq 'qlik'").id -roles "RootAdmin"

Write-Log -Message "Adding npservice user to user directory QLIKLOCAL"
$json = (@{userId = "npservice";
            userDirectory = "QLIKLOCAL";
            name = "npservice";
        } | ConvertTo-Json -Compress -Depth 10 )
Invoke-QlikPost "/qrs/user" $json | Out-Null

Write-Log -Message "Granting RootAdmin role to npservice user"
Update-QlikUser -id $(Get-QlikUser -full -filter "name eq 'npservice'").id -roles "RootAdmin" | Out-Null

Write-Log -Message "Importing extensions..."
If (Test-Path "C:\installation\extensions\") {
    gci c:\\installation\\extensions\\*.zip | foreach { Write-Log -Message "Importing extension $($_.FullName)"; Import-QlikExtension -ExtensionPath $_.FullName | Out-Null}
}

If (Test-Path "C:\shared-content\extensions\") {
    gci c:\\shared-content\\extensions\\*.zip | foreach { Write-Log -Message "Importing extension $($_.FullName)"; Import-QlikExtension -ExtensionPath $_.FullName | Out-Null }
}

Write-Log -Message "Importing applications..."
If (Test-Path "C:\installation\apps\") {
    gci c:\\installation\\apps\\*.qvf | foreach { Write-Log -Message "Importing application $($_.BaseName)"; Import-QlikApp -name $_.BaseName -file $_.FullName -upload | Out-Null }
}

    Write-Log -Message "Importing applications..."
    $apps = gci c:\shared-content\apps\ -Directory
    foreach ($subDirectory in $apps)
    {
        $encodeDirectory = [System.Web.HttpUtility]::UrlEncode($subDirectory);
        $streams = $(Get-QlikStream -filter "name eq '$($encodeDirectory)'").name
        if ($streams -ne $subDirectory) {
            Write-Log -Message "Creating stream $($subDirectory)"
            New-QlikStream $subDirectory | Out-Null;
            $streamId = $(Get-QlikStream -filter "name eq '$($encodeDirectory)'").id
            $systemRuleJson = (@{
                name = "Grant everyone access to $subDirectory";
                category = "Security";
                rule = '((user.name like "*"))';
                type = "Custom";
                resourceFilter = "Stream_$streamId";
                actions = 34;
                ruleContext = "QlikSenseOnly";
                disabled = $false;
                comment = "Stream access";} | ConvertTo-Json -Compress -Depth 10)
            Write-Log -Message "Creating system rule Grant everyone access to $subDirectory"
            Invoke-QlikPost "/qrs/systemrule" $systemRuleJson | Out-Null;
    }
    $files = gci C:\shared-content\apps\$subDirectory\*.qvf -File
    foreach ($file in $files)
        {
            $streamId = $(Get-QlikStream -filter "name eq '$($encodeDirectory)'").id
            $encode = [System.Web.HttpUtility]::UrlEncode($file.BaseName)
            Write-Log -Message "Importing application $encode"
            Import-QlikApp -name $encode -file $file.FullName -upload;
            Write-Log -Message "Publishing applciation $($encode) to stream $subDirectory"
            publish-qlikapp -id $(get-qlikapp -filter "name eq '$($encode)'").id -stream $streamId -name $encode | Out-Null
            }
    }
Write-Log -Message "Adding Websocket Origin White List"
Update-QlikVirtualProxy -id $(Get-QlikVirtualProxy -filter "description eq 'Central Proxy (Default)'").id -websocketCrossOriginWhiteList "qmi-ad-qs.qliklocal.net"| Out-Null

Start-Sleep -s 10

Write-Log -Message "Enabling listening on HTTP (port 80)"
Get-QlikProxy -filter "serverNodeConfiguration.Name eq 'Central'" | Update-QlikProxy -AllowHttp 1 | Out-Null

IF (-Not(Test-Path "c:\users\vagrant\Documents\WindowsPowerShell"))
{
    New-Item "C:\Users\vagrant\Documents\WindowsPowerShell\" -ItemType Directory
}
echo 'function qsgrep($pattern,$service,$ext="txt") {Get-ChildItem "C:\ProgramData\Qlik\Sense\Log\$service" -Filter *.$ext -Recurse | Select-String $pattern}' > "C:\Users\vagrant\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1"

Write-Log -Message "Exporting Qlik Sense certificates for nPrinting"
export-qlikcertificates -machineNames qmi-ad-np.qliklocal.net -includeSecretsKey -exportFormat Windows | Out-Null
