﻿<#
Module:             ad-create
Author:             Clint Carr
Modified by:        -
Modification History:
 - Changed process to use DSc
 - Added Logging
 - Added comments
last updated:       11/17/2017
Intent: Creates active Directory Domain qliklocal.net
#>

 $ComputerName = $env:COMPUTERNAME  
 $Password = "Pass@word1"  
 $DomainName = "qliklocal.net"  
 $MOFfiles = "c:\users\vagrant\documents"  

 $Cred = ConvertTo-SecureString -String $Password -Force -AsPlainText  
 $DomainCredential = New-Object System.Management.Automation.PSCredential ("$(($DomainName -split '\.')[0])\Administrator", $Cred)  
 $DSRMpassword = New-Object System.Management.Automation.PSCredential ('No UserName', $Cred)  
 $vagrantPass = ConvertTo-SecureString -String "vagrant" -Force -AsPlainText  
 $RemoteAdministratorCred = New-Object System.Management.Automation.PSCredential -ArgumentList $env:ComputerName\vagrant, $vagrantPass
 $CimSession = New-CimSession -ComputerName $ComputerName -Credential $RemoteAdministratorCred -Name $ComputerName  
 Configuration NewActiveDirectoryConfig {  
   param (  
     [Parameter(Mandatory)]   
     [PSCredential]$DomainCredential,  
     [Parameter(Mandatory)]   
     [PSCredential]$DSRMpassword  
   )  
   Import-DscResource –ModuleName xActiveDirectory, xPSDesiredStateConfiguration,xNetworking  
   Node $ComputerName {  
     WindowsFeature ActiveDirectory {  
       Ensure = 'Present'  
       Name = 'AD-Domain-Services'  
     }  
     WindowsFeature ActiveDirectoryTools {  
       Ensure = 'Present'  
       Name = 'RSAT-ADDS'  
       DependsOn = "[WindowsFeature]ActiveDirectory"  
     }  
     WindowsFeature DNSServerTools {  
       Ensure = 'Present'  
       Name = 'RSAT-DNS-Server'  
       DependsOn = "[WindowsFeature]ActiveDirectoryTools"  
     }  
     WindowsFeature ActiveDirectoryPowershell {  
       Ensure = "Present"  
       Name  = "RSAT-AD-PowerShell"  
       DependsOn = "[WindowsFeature]DNSServerTools"  
     }  

     xADDomain RootDomain {  
       Domainname = $DomainName  
       SafemodeAdministratorPassword = $DSRMpassword  
       DomainAdministratorCredential = $DomainCredential  
       DependsOn = "[WindowsFeature]ActiveDirectory", "[WindowsFeature]ActiveDirectoryPowershell"  
     }  

     LocalConfigurationManager {        
       ActionAfterReboot = 'ContinueConfiguration'        
       ConfigurationMode = 'ApplyOnly'        
       RebootNodeIfNeeded = $true        
     }        
   }  
 }  
 $ConfigurationData = @{  
   AllNodes = @(  
     @{  
       NodeName = $ComputerName  
       PSDscAllowPlainTextPassword = $true  
       DomainName = $DomainName  
      }  
   )  
 }  
 NewActiveDirectoryConfig -DSRMpassword $DSRMpassword -DomainCredential $DomainCredential -OutputPath $MOFfiles -ConfigurationData $ConfigurationData  
 Start-DscConfiguration -Path $MOFfiles -Verbose -CimSession $CimSession -Wait -Force 