<#
Module:             qs-install.ps1
Author:             Clint Carr
Modified by:        -
Modification History:
 - Added Logging
 - Added comments
 - Set up error checking
last updated:       10/11/2017
Intent: Install Qlik Sense
#>
Write-Log -Message "Starting qs-install.ps1"
$config = (Get-Content c:\vagrant\files\qs-cfg.json -raw) | ConvertFrom-Json
Write-Log -Message "Installing Shared Persistence"
If (Test-Path "C:\installation\Qlik_Sense_setup.exe")
    {
        Write-Log -Message "Installing Qlik Sense Server from c:\installation"
        Unblock-File -Path C:\installation\Qlik_Sense_setup.exe
        Invoke-Command -ScriptBlock {Start-Process -FilePath "c:\installation\Qlik_Sense_setup.exe" -ArgumentList "-s -log c:\installation\logqlik.txt dbpassword=$($config.sense.PostgresAccountPass) hostname=$($config.servers.name) userwithdomain=$($config.sense.serviceAccount) password=$($config.sense.serviceAccountPass)  spc=c:\installation\sp_config.xml" -Wait -PassThru}  | Out-Null
    }
elseIf (Test-Path "c:\shared-content\binaries\Qlik_Sense_setup.exe")
    {
        Write-Log -Message "Installing Qlik Sense Server from c:\shared-content\binaries"
        Unblock-File -Path C:\shared-content\binaries\Qlik_Sense_setup.exe
        Invoke-Command -ScriptBlock {Start-Process -FilePath "c:\shared-content\binaries\Qlik_Sense_setup.exe" -ArgumentList "-s -log c:\installation\logqlik.txt dbpassword=$($config.sense.PostgresAccountPass) hostname=$($config.servers.name) userwithdomain=$($config.sense.serviceAccount) password=$($config.sense.serviceAccountPass)  spc=c:\installation\sp_config.xml" -Wait -PassThru} | Out-Null
    }