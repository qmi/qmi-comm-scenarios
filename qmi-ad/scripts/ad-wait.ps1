<#
Module:             ad-wait
Author:             Clint Carr
Modified by:        -
Modification History:
 - Added Logging
 - Added comments
 - Set up error checking
last updated:       10/11/2017
Intent: Wait until we can access the AD. this is needed to prevent errors like:
        Unable to find a default server with Active Directory Web Services running.
#>
Write-Log -Message "Starting ad-wait.ps1"
while ($true) {
    try {
        Get-ADDomain | Out-Null
        break
    } catch {
        Start-Sleep -Seconds 20
        Write-Log -Message "Waiting for machine to Finish Provisioning. Waiting for 20 seconds."
    }
}

Set-ADDefaultDomainPasswordPolicy -Identity qliklocal.net -ComplexityEnabled $false | Out-Null
$password = ConvertTo-SecureString -AsPlainText 'Qlik1234' -Force
$name = 'qService'
Write-Log -Message "Creating qService domain account"
New-ADUser  -name $name `
            -UserPrincipalName "$name@qliklocal.net" `
            -DisplayName 'QlikServiceAccount' `
            -AccountPassword $password `
            -Enabled $true `
            -PasswordNeverExpires $true | Out-Null

$name = 'npService'
Write-Log -Message "Creating npService domain account"
New-ADUser  -name $name `
            -UserPrincipalName "$name@qliklocal.net" `
            -DisplayName 'nPrintingAccount' `
            -AccountPassword $password `
            -Enabled $true `
            -PasswordNeverExpires $true | Out-Null

$name = 'qlik'
Write-Log -Message "Creating qlik domain account"
New-ADUser  -name $name `
            -UserPrincipalName "$name@qliklocal.net" `
            -DisplayName 'QlikAccount' `
            -AccountPassword $password `
            -Enabled $true `
            -PasswordNeverExpires $true | Out-Null