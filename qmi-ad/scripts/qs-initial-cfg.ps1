<#
Module:             qs-initial-cfg
Author:             Clint Carr
Modified by:        -
Modification History:
 - Added Logging
 - Added comments
 - Set up error checking
last updated:       10/11/2017
Intent: Initial config before installation of Qlik Sense
#>
$config = (Get-Content c:\vagrant\files\qs-cfg.json -raw) | ConvertFrom-Json
$computer = $config.servers | where { $_.name -eq $env:computername }

$config.servers | foreach { Write "$($_.ip) $($_.name)" | Out-File "C:\Windows\System32\drivers\etc\hosts" -Append -Encoding "UTF8" }

Write-Log -Message "Creating directory for Shared Persistence"
New-Item -ItemType directory -Path C:\QlikShare | Out-Null
Write-Log -Message "Create file share"
New-SmbShare -Name QlikShare -Path C:\QlikShare -FullAccess everyone | Out-Null

Write-Host "Installing NuGet package provider"
Get-PackageProvider -Name NuGet -ForceBootstrap
Write-Host "Installing DesiredState module"
Install-Module -Name xPSDesiredStateConfiguration -Confirm:$false -Force
Write-Host "Installing Networking module"
Install-Module -Name xNetworking -Confirm:$false -Force
Write-Host "Installing SMB module"
Install-Module -Name xSmbShare -Confirm:$false -Force
Write-Host "Installing Qlik-CLI module"
Install-Module -Name Qlik-CLI -Confirm:$false -Force
Write-Host "Installing Qlik-DSC module"
Install-Module -Name QlikResources -Confirm:$false -Force

cmd.exe /c winrm set winrm/config `@`{MaxEnvelopeSizekb=\`"8192\`"`}

Write-Log -Message "Adding Service Account to Local administrators group"
Add-LocalGroupMember -Group "Administrators" -Member "qliklocal\qservice", "qliklocal\npservice", "qliklocal\qlik"  | Out-Null

Write-Log -Message "Disabling Firewall"
Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False  | Out-Null
