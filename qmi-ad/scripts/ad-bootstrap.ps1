Get-PackageProvider -Name NuGet -ForceBootstrap
Install-Module -Name xActiveDirectory -Confirm:$false -Force
Install-Module -Name xComputerManagement -Confirm:$false -Force
Install-Module -Name xNetworking -Confirm:$false -Force
Install-Module -Name xStorage -Confirm:$false -Force
Install-Module -Name xPendingReboot -Confirm:$false -Force
Install-Module -Name xPSDesiredStateConfiguration -Confirm:$false -Force
