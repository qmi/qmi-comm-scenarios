# Windows 2016 Domain
## Description
This scenario creates a Windows 2016 Active Directory domain called *qliklocal.net* that contains a single domain controller.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-ad            | 192.168.56.3      | Domain Controller, DNS Server |


## URLs
| Name | URL | Purpose
|------|-----|---------


## Users
| Name | Password |
|------|-----|
|qliklocal\qlik| Qlik1234|

## Purpose
The purpose of this scenario is to demonstrate Active Directory

## What is installed
### Software
1. Active Directory

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.5|02 Aug 2018|
