echo 'Installing Docker'

sudo yum -y install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

sudo yum-config-manager -y \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

sudo yum -y install docker-ce

echo 'Starting docker daemon'
sudo systemctl start docker

echo 'Installing docker-compose'
curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo 'Installing Docker Machine'
curl -L https://github.com/docker/machine/releases/download/v0.13.0/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine &&
    chmod +x /tmp/docker-machine &&
    sudo cp /tmp/docker-machine /usr/local/bin/docker-machine

sudo usermod -aG docker vagrant