echo 'Updating CentOS'
sudo yum -y update

echo 'Installing git'
sudo yum -y install git

echo 'Adding nodes to /etc/hosts'
echo "192.168.56.53 qmi-centos-n1" >> /etc/hosts | sudo tee -a /etc/hosts
echo "192.168.56.54 qmi-centos-n2" >> /etc/hosts | sudo tee -a /etc/hosts
echo "192.168.56.55 qmi-centos-n3" >> /etc/hosts | sudo tee -a /etc/hosts
