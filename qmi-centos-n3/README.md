# Three Node CentOS with Docker
## Description
This scenario provisions three CentOS machines with Docker CE, docker-compose and docker-machine.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-centos-n1 | 192.168.56.53    | CentOS with Docker node 1 |
| qmi-centos-n2 | 192.168.56.54    | CentOS with Docker node 2 |
| qmi-centos-n3 | 192.168.56.55    | CentOS with Docker node 3 |

## Users
| Name | Password |
|------|-----|
|vagrant|vagrant|

## Connection
Please use __vagrant ssh__ to connect to the server

## Purpose
This scenario is a basic installation of CentOS with Docker preinstalled.

## What is installed
### Software
1. CentOS
2. Docker CE
3. Docker-compose
4. Docker-machine

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.0|16 Nov 2017|
