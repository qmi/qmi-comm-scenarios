# Ubuntu
## Description
Blank Ubuntu 16.04 Image.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-ubuntu | 192.168.56.52    | Ubuntu server |

## URLs
| Name | URL | Purpose
|------|-----|---------

## Users
| Name | Password |
|------|-----|
|vagrant|vagrant|

## Connection
Please use __vagrant ssh__ to connect to the server

## Purpose
This scenario is a basic installation of Ubuntu.

## What is installed
### Software
1. Ubuntu 18.04

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Marcus Spitzmiller|1.0|23 Oct 2017|
