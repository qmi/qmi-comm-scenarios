sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get -y update
# sudo apt-get -y install postgresql

sudo apt-get install postgresql-9.6 -y

sudo cp /vagrant/files/postgresql.conf /etc/postgresql/9.6/main/postgresql.conf

echo "Restarting postgresql service"
systemctl restart postgresql

echo "Changing to password"
sudo -u postgres psql -p 4432 postgres -c "ALTER USER postgres WITH ENCRYPTED PASSWORD 'Qlik1234'"
sudo -u postgres psql -p 4432 postgres -c "CREATE EXTENSION adminpack";
sudo -u postgres createdb QSR -O postgres
sudo -u postgres psql -p 4432 postgres -c "CREATE ROLE qliksenserepository LOGIN SUPERUSER PASSWORD 'Qlik1234'"

echo "Configuring postgresql.conf"
sudo echo "listen_addresses = '*'" >> /etc/postgresql/9.6/main/postgresql.conf

# Edit to allow socket access, not just local unix access
echo "Patching pg_hba to change -> socket access"
sudo echo "host all all 0.0.0.0/0  md5" >> /etc/postgresql/9.6/main/pg_hba.conf
sudo service postgresql restart

