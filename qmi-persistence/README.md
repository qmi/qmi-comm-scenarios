# Ubuntu server for shared persistence content with a single node Qlik Sense Server
## Description
This scenario offers two servers, an Ubuntu server that hosts the Qlik Sense file share (SMB3.0) and the postgreSQL database.  And a Single Windows 2016 Server hosting Qlik Sense.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-persistence   | 192.168.56.23     | File Share, Database Server |
| qmi-qs-ps2        | 192.168.56.24     | Qlik Sense Server |

## URLs
| Name | URL | Purpose
|------|-----|---------
|QMC|http://qmi-qs-ps2/qmc | QMC
|hub|http://qmi-qs-ps2/hub | hub

## Users
| Name | Password |
|------|-----|
|.\qlik| Qlik1234|

## Purpose
The purpose of this scenario is to demonstrate the ability to remove the shared persistence components from the Qlik Sense software, in addtion in using a Linux server for the file share the scenario demonstrates hosting the files on a non Windows SMB3.0 file share (simulating Network Attached Storage [NAS])

## What is installed
### Software
1. Qlik Sense Server
2. Ubuntu Server with postgreSQL 9.6

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.0|8 Aug 2017|
