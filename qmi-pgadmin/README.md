# PGAdmin4
## Description
PGAdmin4 docker container.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-pgadmin | 192.168.56.56    | PGAdmin |

## Users
| Name | Password |
|------|-----|
| pgadmin4@pgadmin.org|admin|

## Connection
From your hosts browser connect to __http://qmi-pgadmin:5050__

## Purpose
To allow PostgreSQL management.

## What is installed
### Software
1. Ubuntu 18.04
2. Docker CE
3. PGAdmin container

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.0|13 Dec 2017|
