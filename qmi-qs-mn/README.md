# Qlik Sense in a multi-node
## Description
This scenario provisions three servers, two Qlik Sense Servers in multi-node and an NGINX load balancer.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-qs-mn       | 192.168.56.8     | Qlik Sense Central Node |
| qmi-qs-mn02 | 192.168.56.9 | Qlik Sense Rim Node|
| qmi-nginx | 192.168.56.15 | Load Balancer

## URLs
| Name | URL | Purpose
|------|-----|---------
|QMC|http://qmi-qs-mn/qmc | QMC
|hub|http://qmi-qs-mn/hub | Central Node hub
|hub|http://qmi-qs-mn02/hub | Rim Node hub
|hub|http://qmi-nginx | Load Balancer

## Users
| Name | Password |
|------|-----|
|.\qlik| Qlik1234|

## Purpose
The purpose of this scenario is to be able to demonstrate multinode servers behind a commonly deployed Load Balancer (NGINX), this scenario also demonstrates url re-writing, ensuring that Windows Authentication does not use port 4248 instead it uses 80 alongside Qlik Sense itself.

## What is installed
### Software
1. Qlik Sense Server
2. NGINX

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.0|1 Aug 2017|
