# qmi-comm-scenarios

QMI Scenarios in Community

## Requirements
* Virtualbox
* Vagrant
* Git

## Repository dependencies
* Public scripts: https://github.com/Qlik-Partner-Tools/qlik-automation-core
* Private scripts: https://gitlab.com/qmi/qlik-automation-plus

It'll get resolved when executing **init-shared-content.sh**

## Get started
Initialise folder "shared-content". Execute following shell script for initialisation


Linux and OSX
```
./init-shared-content.sh

```
Windows
```
./init-shared-content.bat

```