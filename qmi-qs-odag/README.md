# Qlik Sense ODAG
## Description
This scenario provisions Qlik Sense with ODAG enabled by default.

| Servername        | Server IP         | Purpose |
|-------------------|-------------------|---------|
| qmi-qs-odag       | 192.168.56.11     | Qlik Sense |

There are two ODAG demos in this scenario:

**Sales and Inventory**
- One level of detail.
- It uses QVDs as datasource.
- Folder connector is already created during provision.
- This demo doesn't need any aditional configuration.
- ODAG link created

**New York City Taxi**
- Two levels of apps for details.
- It uses public Google BigQuery repository as datasource.
- Applicactions bring some Geonalytics visualisations.
- Simba ODBC connector is already created and linked to this application
- ODAG link created
- IMPORTANT: Prior to demo, ODBC driver needs to be configure to use a Google Account with Google BigQuery and billing is enabled. Follow points 1-7 in document C:/installation/NYC-Setup.pdf

*Script Video*

![](https://d7ipctdjxxii4.cloudfront.net/others/resources/qmi-qs-odag.gif)

## URLs
| Name | URL | Purpose
|------|-----|---------
|QMC|http://qmi-qs-odag/qmc | QMC
|hub|http://qmi-qs-odag/hub | hub

## Users
| Name | Password |
|------|-----|
|qliklocal\qlik| Qlik1234|

## Purpose
Odag demos.

## What is installed
### Software
1. Qlik Sense Server
2. Geoanalytics
3. Simba ODBC driver for Google BigQuery (NYC Taxi demo)

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.0|1 Aug 2017|
|Manuel Romero|1.0|31 Aug 2017|
