# CentOS
## Description
Blank CentOS 7.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-centos | 192.168.56.51    | Blank CentOS |

## URLs
| Name | URL | Purpose
|------|-----|---------

## Users
| Name | Password |
|------|-----|
|vagrant|vagrant|

## Connection
Please use __vagrant ssh__ to connect to the server

## Purpose
This scenario is a basic installation of CentOS.

## What is installed
### Software
1. CentOS

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Marcus Spitzmiller|1.0|18 Oct 2017|
