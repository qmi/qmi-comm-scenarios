# disable firewall
Write-Host "Disabling Firewall"
Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False

if (-Not (Test-Path C:\installation\SSMS-Setup-ENU.exe)) {
Write-Host "Downloading Microsoft SQL Management Studio - Approx 880 Mb"
Invoke-WebRequest "https://go.microsoft.com/fwlink/?linkid=849819" -OutFile "C:\installation\SSMS-Setup-ENU.exe"
}

New-Item -ItemType directory -Path C:\SQL