Write-Host "SQL Management Tools will finish installation in approx 5 minutes."
start-sleep -s 60

if (-Not (Test-Path C:\installation\WideWorldImportersDW-Full.bak)) {
Write-Host "Downloading Wide World Importers DW Sample Data"
# Invoke-WebRequest "https://github.com/Microsoft/sql-server-samples/releases/download/wide-world-importers-v1.0/WideWorldImportersDW-Full.bak" -OutFile "C:\installation\WideWorldImportersDW-Full.bak"
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 
(New-Object System.Net.WebClient).DownloadFile("https://github.com/Microsoft/sql-server-samples/releases/download/wide-world-importers-v1.0/WideWorldImportersDW-Full.bak", "C:\installation\WideWorldImportersDW-Full.bak")

}

while (!(Test-Path "C:\installation\WideWorldImportersDW-Full.bak")) { Start-Sleep 10 }
Write-Host "Restoring Wide World Importers DW"
SQLCMD -E -S qmi-db\SQLEXPRESS -Q "RESTORE DATABASE WideWorldImportersDW FROM DISK='c:\installation\WideWorldImportersDW-Full.bak' WITH MOVE 'WWI_Primary' TO 'c:\SQL\WWI_Primary.mdf', MOVE 'WWI_Log' TO 'c:\SQL\WWI_Log.ldf', MOVE 'WWI_UserData' TO 'c:\SQL\WMI_UserData.ndf', MOVE 'WWIDW_InMemory_Data_1' TO 'c:\SQL\WWIDW_InMemory_Data_1_1'"

& c:\shared-content\scripts\modules\q-provisioned.ps1

Write-Host "============================"
Write-Host "Server provisioning complete"
Write-Host "============================"