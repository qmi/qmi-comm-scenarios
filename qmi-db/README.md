# Microsoft SQL Server
## Description
Windows 2016 Server with both SQL Server 2016.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-db     | 192.168.56.20    | Database |

## Purpose
This scenario provides an instance of SQL Server that contains sample data, using this scenario you can connect via ODBC to demonstrate loading data.  Connect to SQL with user SA with the password Qlik1234.

## What is installed
### Software
1. Microsoft SQL Server Express 2016

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.1|7 Sept 2017|
