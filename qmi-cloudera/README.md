# Cloudera Quickstart Docker
## Description
This scenario offers a single Ubuntu server running Cloudera/Quickstart in a container.  From here, Cloudera can be configured and then used as a source of data for Qlik Products.   __Updated to Cloudera 5.12__

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-cloudera | 192.168.56.30    | Cloudera|

## URLs
| Name | URL | Purpose
|------|-----|---------
|Cloudera Hue|http://qmi-cloudera:8888/| Hue|
|Cloudera-Manager|http://qmi-cloudera:7180/| Cloudera-Manager|
|Cloudera Search|http://qmi-cloudera:8983| Solr |

* Note - Cloudera Hue will only be accessible once you have enabled it via Cloudera-Manager.
* Note - Please ensure you disable the NTP warnings in Cloudera-Manager (if seen)

## Access
The provisioning of Cloudera has the following manual steps.  Once QMI has completed perform the following:
1. Launch your terminal application (Command/PowerShell/Bash)
2. Browse to QlikMachineImages/qmi-cloudera
3. enter __vagrant ssh qmi-cloudera__
4. Enter __./startCloudera.sh__ 
5. From the docker shell (root@quickstart) enter __/home/cloudera/cloudera-manager --express__
6. Leave the shell open or docker will close and thus Cloudera will be closed
7. Once you are within Cloudera Manager start all services

Cloudera will launch and be accessible from your host.

## Users
| Name | Password |
|------|-----|
|cloudera|cloudera|

## Purpose
The purpose of this scenario is to provision a the Cloudera Quickstart.

## What is installed
### Software
1. Ubuntu Server 16.04
2. Cloudera Quickstart

## Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.4|10 oct 2017|
