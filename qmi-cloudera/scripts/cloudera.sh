echo 'Downloading Container - 4Gb - may take some time.'
mkdir ~/cloudera

wget https://downloads.cloudera.com/demo_vm/docker/cloudera-quickstart-vm-5.12.0-0-beta-docker.tar.gz -O ~/cloudera/cloudera-quickstart-vm-5.12.0-0-beta-docker.tar.gz -q --show-progress

echo 'Extracting tar'
tar xzf ~/cloudera/cloudera-quickstart-vm-*-docker.tar.gz -C ~/cloudera

echo 'Importing container'
sudo docker import - cloudera/quickstart:latest < ~/cloudera/cloudera-quickstart-vm-*-docker/*.tar

cp /vagrant/files/startCloudera.sh .
chmod 777 ./startCloudera.sh

echo 'run ./startCloudera.sh to run the Docker container'


