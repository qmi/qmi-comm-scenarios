echo 'Updating Ubuntu'
sudo apt-get -y update
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get -y update
echo 'Installing Docker'
sudo apt-get -y install docker-ce
echo 'Installing Docker Compose'
sudo apt-get -y install docker-compose
echo "installing ntp"
sudo apt install ntp
echo starting ntp
sudo service ntp restart

sudo systemctl restart ntp.service

sudo chmod 777 /vagrant/scripts/cloudera.sh


sudo gpasswd -a $USER docker