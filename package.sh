#!/bin/bash
SCENARIOS_TO_PACKAGE="*"

# ElasticSearch database 
ES_URL="http://172.21.2.16/elasticsearch/qmicommunity/scenario/"
ES_USERNAME="user" 


if [ -z "$1" ]
  then
    echo "All scenarios will be packaged"  
else 
    SCENARIOS_TO_PACKAGE=$1
    echo "Package just scenario: $SCENARIOS_TO_PACKAGE"
fi

if [ -z "$ES_PASSWORD" ]; then
    echo "Provide ElasticSearch passoword through variable ES_PASSWORD."
    echo "Execute: ES_PASSWORD=thepassword package.sh"
    exit
fi

branch="master"
rm -rf ./qBuild
mkdir ./qBuild

git archive --format zip --output ./qBuild/qlik-vagrant-bundle.zip $branch

pushd ./qBuild

zip -d qlik-vagrant-bundle.zip "init-shared-content.sh"
zip -d qlik-vagrant-bundle.zip "README.md"
zip -d qlik-vagrant-bundle.zip ".gitignore"
zip -d qlik-vagrant-bundle.zip "package.sh"

unzip qlik-vagrant-bundle.zip
rm -fr qlik-vagrant-bundle.zip
now=`date -u +"%Y-%m-%dT%H:%M:%SZ"`
for i in $SCENARIOS_TO_PACKAGE; do 
    version=`cat "$i/scenario.json" | jq '.version'`
    fileName="${i%/}_v$version.zip"
    zip -r $fileName "$i"; 
    downloadLink="https://d7ipctdjxxii4.cloudfront.net/community/scenarios/gear@qlik.com/$fileName"
    more="{\"owner\": \"gear@qlik.com\",\"published\": true, \"lastUpdate\": \"$now\", \"downloadLink\": \"$downloadLink\"}"
    json=`cat "$i/scenario.json" | jq ". += $more" -c`
    echo "------"
    echo $json
    curl -u $ES_USERNAME:$ES_PASSWORD -H 'Content-Type: application/json' -X POST $ES_URL -d"$json"
done

# Scenario Zips to S3
mkdir scenarios
mv *.zip scenarios
aws s3 sync scenarios s3://qmidata/community/scenarios/gear@qlik.com


# MD file to S3
find ./qmi* -name '*.md' | cpio -pdm ./md
aws s3 sync md s3://qmidata/community/md 

popd