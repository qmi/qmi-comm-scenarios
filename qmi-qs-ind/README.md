# Qlik Sense Industry Solutions
## Description
This scenario provides applications that are created by the Industry Solutions Team.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-qs-ind        | 192.168.56.13     | Qlik Sense |

## URLs
| Name | URL | Purpose
|------|-----|---------
|QMC|http://qmi-qs-ind/qmc | QMC
|hub|http://qmi-qs-ind/hub | hub
|web connectors|http://qmi-qs-ind:5555/web | web connectors

## Users
| Name | Password |
|------|-----|
|.\qlik| Qlik1234|

## Purpose
The purpose of this scenario is to be able to demonstrate the applications provided by the Industry Solutions Team.

## What is installed
### Software
1. Qlik Sense Server
2. GeoAnalytics Base 5.8.1
3. GeoAnalytics Plus 1.7.0
4. Qlik Web Connectors

## Qlik Sense Extensions
* D3 Improved Radar Chart
* qsSimpleKPI
* qsVariable
* d3-calendar-heatmap
* Qlik-Sense-D3-Visualization-Library
* SheetNavigation
* QlikSenseD3zoomableSunburst
* br.com.clever.wordcloud
* SenseSankey

### Qlik Sense Applications
* Retails Sales
* CEM Communications
* Customer Service & Call Center
* Workforce Management with GeoAnalytics
* Procurement
* Call Detail Analysis
* Telco Churn
* OEE
* Supply Chain - Inventory and Product Availability
* General Ledger
* Market 360
* Qlik2Go - Expense Management and Cost Transparency
* Qlik2Go - Workforce Performance and Talent Analytics
* Executive Dashboard
* IT Asset Management
* Travel Insights
* Qlik Travel Insights
* Project Management
* Social Media Buzz
* CEM Energy Utilities
* CEM Finance
* Connecting Customer Experience to Sales with Analytics
* CEM Travel
* CEM Consumer Goods
* CEM Automotive
* CEM Lenovo
* CEM Computers
* Retail Fuel Analysis
* Smart Meter Selections
* Smart Meter Details
* Oil and Gas Well Performance
* Linear Regression
* Retailer EPOS Data Analytics - Summit 2015
* Asset Management - Deterioration
* Asset Management - Dynamic Assets
* Gambling and Gaming v2.2
* Qlik2Go  Workforce Performance, Talent & Gender Analytics for Services
* Law Attorney Dashboard_DemoNoScript
* Qlik2Go+ Workforce Performance and Talent Analytics for Services
* Audit Management and Control
* Qlik2Go+ Resource Planning for Services


### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.0|1 Aug 2017|