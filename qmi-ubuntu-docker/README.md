# Ubuntu with Docker
## Description
Ubuntu with Docker CE running with docker-compose.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-ubuntu-docker | 192.168.56.29    | Ubuntu with Docker |

## Users
| Name | Password |
|------|-----|
|vagrant|vagrant|

## Connection
Please use __vagrant ssh__ to connect to the server

## Purpose
This scenario is a basic installation of Ubuntu with Docker preinstalled.

## What is installed
### Software
1. Ubuntu 18.04
2. Docker CE
3. Docker Compose

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Marcus Spitzmiller|1.1|24 Oct 2017|
