# Qlik Sense Ticketing
## Description
Qlik Sense server with Ticketing Authentication.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-qs-ticket     | 192.168.56.25   | Qlik Sense |

## URLs
| Name | URL | Purpose
|------|-----|---------
|QMC|http://qmi-qs-ticket/qmc | QMC
|hub|http://qmi-qs-ticket/hub | hub
|hub|http://qmi-qs-ticket/google/hub| Google Auth
|hub|https://qmi-qs-ticket/o365/hub| Office 365 Auth

## Users
| Name | Password |
|------|-----|
|.\qlik| Qlik1234|

## Purpose
This scenario allows for authentication via either Google (OAUTH), or Microsoft office 365.

## What is installed
### Software
1. Qlik Sense Server

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Manuel Romero|1.0|8 Aug 2017|
