#!/bin/bash

GITFOLDER="git-shared-content"

# Public scripts
git clone https://github.com/Qlik-Partner-Tools/qlik-automation-core.git $GITFOLDER/qlik-automation-core
pushd $GITFOLDER/qlik-automation-core
git checkout .
git pull origin master
popd

# Private scripts
git clone git@gitlab.com:qmi/qlik-automation-plus.git $GITFOLDER/qlik-automation-plus
pushd $GITFOLDER/qlik-automation-plus
git checkout .
git pull origin master
popd

# Add/Overwrite shared-content
echo "------ Creating shared-content folder from PUBLIC content ------- "
mkdir -p ../shared-content
cp -Rf $GITFOLDER/qlik-automation-core/* ../shared-content
echo "------ Appending/Overwriting shared-content folder with PRIVATE content ------- "
cp -Rf $GITFOLDER/qlik-automation-plus/* ../shared-content
#remove git connection
rm -fr ../shared-content/.git


echo ""
echo ""
echo "-----------------------------------------------------"
echo "------ DEVELOPER!! This is VERY IMPORTANT!!! ------- "
echo "-----------------------------------------------------"
echo ""
echo "Folder shared-content is disconnected from GIT."
echo "Changes to scripts need to be commited/pushed to the specific repository, then run this script again to update 'shared-content' folder."
echo "==> Public scripts: $GITFOLDER/qlik-automation-core"
echo "==> Private scripts: $GITFOLDER/qlik-automation-plus"
echo ""
echo "-----------------------------------------------------"
echo ""
