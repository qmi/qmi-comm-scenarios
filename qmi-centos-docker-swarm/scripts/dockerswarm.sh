echo 'Initializing Docker Swarm'

#initialize docker swarm, server 1 becomes a manager by default
sudo docker swarm init --advertise-addr 192.168.56.61

#join node2 to the swarm
sshpass -p vagrant ssh -o StrictHostKeyChecking=no 192.168.56.62 sudo docker swarm join --token $(sudo docker swarm join-token worker --quiet) 192.168.56.61:2377
#sshpass -p vagrant ssh -o StrictHostKeyChecking=no qmi-centos-docker-swarm2 sudo docker swarm join --token $(sudo docker swarm join-token worker --quiet) qmi-centos-docker-swarm1:2377

#join node2 to the swarm
sshpass -p vagrant ssh -o StrictHostKeyChecking=no 192.168.56.63 sudo docker swarm join --token $(sudo docker swarm join-token worker --quiet) 192.168.56.61:2377
#sshpass -p vagrant ssh -o StrictHostKeyChecking=no qmi-centos-docker-swarm2 sudo docker swarm join --token $(sudo docker swarm join-token worker --quiet) qmi-centos-docker-swarm1:2377
