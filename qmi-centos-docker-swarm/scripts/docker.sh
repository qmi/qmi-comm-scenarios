echo 'Installing Docker'

sudo yum -y install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

sudo yum-config-manager -y \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

sudo yum -y install docker-ce

echo 'Starting docker daemon'
sudo systemctl start docker
sudo usermod -aG docker $USER

echo 'Installing docker-compose'
curl -L https://github.com/docker/compose/releases/download/1.17.0-rc1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

