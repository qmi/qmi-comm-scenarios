# CentOS with Docker Swarm
## Description
CentOS and Docker Swarm

| Servername        | Server IP         | Purpose |
|-------------------|-------------------|---------|
| qmi-centos-docker-swarm | 192.168.56.61    | CentOS with Docker Swarm Node 1 |
| qmi-centos-docker-swarm2 | 192.168.56.62    | CentOS with Docker Swarm Node 2 |
| qmi-centos-docker-swarm3 | 192.168.56.63    | CentOS with Docker Swarm Node 3 |

## Users
| Name | Password |
|------|-----|
|vagrant|vagrant|

## Connection
Please use __vagrant ssh__ to connect to the server

## Purpose
This scenario is a basic multi-node installation of CentOS with Docker Swarm preinstalled.

## What is installed
### Software
1. CentOS
2. Docker CE
3. Docker Compose
4. Docker Swarm

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Marcus Spitzmiller|1.0|28 Nov 2017|
