# Linux server running ElasticSearch
## Description
This scenario offers a single Ubuntu server running ElasticSearch 6.2.1

| Servername        | Server IP         | Purpose |
|-------------------|-------------------|---------|
| qmi-elasticsearch | 192.168.56.28    | Elastic Search |

## URLs
| Name | URL | Purpose
|------|-----|---------
|ES Root|http://qmi-elasticsearch:9200/|Accessing ES Root Endpoint|
|ES Getting Started|https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html|Assistance with Getting Started|

## Access
SSH is enabled by default and open on port 22, user credentials are used for authentication.

## Users
| Name | Password |
|------|-----|
|vagrant|vagrant|


## Purpose
The purpose of this scenario is to provision a quick ES environment for use when demonstrating Qlik Sense with ElasticSearch as a datasource.

## What is installed
### Software
1. Minimal Ubuntu Server (No GUI)
2. Elasticsearch 6.2.1

## Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Daniel Aherne-McInteer|1.0|28 Aug 2017|
|Clint Carr|1.1|12 Feb 2019|
