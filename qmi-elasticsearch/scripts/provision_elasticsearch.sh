# ELASTICSEARCH
echo 'Installing Java Prerequisites'
# install openjdk-8
apt-get purge openjdk*
add-apt-repository ppa:webupd8team/java -y
apt-get update -y
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
echo 'Installing Java (This may take 10 minutes)'
apt-get install oracle-java8-installer -y >/dev/null

# install ES
echo 'Downloading ES (This may take 5 minutes)'
curl -L -O -s https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.5.2.tar.gz

echo 'Installing ES'
tar -xvf elasticsearch-5.5.2.tar.gz

sudo sysctl -w vm.max_map_count=262144
echo "vagrant soft     nofile         65536" | sudo tee --append /etc/security/limits.conf
echo "vagrant hard     nofile         65536" | sudo tee --append /etc/security/limits.conf
echo "session required pam_limits.so" | sudo tee --append /etc/pam.d/common-session
sudo -H -u vagrant bash -c 'ulimit -n'

# either of the next two lines is needed to be able to access "localhost:9200" from the host os
echo "network.bind_host: 0" >> elasticsearch-5.5.2/config/elasticsearch.yml
echo "network.host: 0.0.0.0" >> elasticsearch-5.5.2/config/elasticsearch.yml
# enable dynamic scripting
echo "script.inline: on" >> elasticsearch-5.5.2/config/elasticsearch.yml
# enable cors (to be able to use Sense)
echo "http.cors.enabled: true" >> elasticsearch-5.5.2/config/elasticsearch.yml
echo "http.cors.allow-origin: /https?:\/\/.*/" >> elasticsearch-5.5.2/config/elasticsearch.yml

#Create Important Folders for storage
sudo mkdir -p /var/data/elasticsearch
sudo chmod 777 /var/data/elasticsearch

sudo mkdir -p /var/log/elasticsearch
sudo chmod 777 /var/log/elasticsearch

#Setup Important Configurations
echo "cluster.name: qmi-elasticsearch" >> elasticsearch-5.5.2/config/elasticsearch.yml
echo "node.name: ${HOSTNAME}" >> elasticsearch-5.5.2/config/elasticsearch.yml
echo "path.data: /var/data/elasticsearch" >> elasticsearch-5.5.2/config/elasticsearch.yml
echo "path.logs: /var/log/elasticsearch" >> elasticsearch-5.5.2/config/elasticsearch.yml


sudo chown -R vagrant:vagrant elasticsearch-5.5.2
echo ' ES Provisioning Complete'