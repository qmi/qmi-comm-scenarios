# CentOS with Docker
## Description
CentOS with Docker CE running with docker-compose.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-centos-docker | 192.168.56.50    | CentOS with Docker |

## Users
| Name | Password |
|------|-----|
|vagrant|vagrant|

## Connection
Please use __vagrant ssh__ to connect to the server

## Purpose
This scenario is a basic installation of CentOS with Docker preinstalled.

## What is installed
### Software
1. CentOS
2. Docker CE
3. Docker-compose
4. Docker-machine

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Marcus Spitzmiller|1.0|18 Oct 2017|
