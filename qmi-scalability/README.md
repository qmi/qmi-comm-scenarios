# Qlik Sense Scalability Tools
## Description
This scenario provides the scalability tools downloaded and installed into c:\scalability.  From here it is a simple task to set up a connection with an existing Qlik Sense environment.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-scalability     | 192.168.56.26    | Scalability testing |

## Purpose
To provide a scenario for testing Qlik Sense.

## What is installed
### Software
1. Qlik Sense Scalability Tools (c:\scalabilityTools)


## Users
| Name | Password |
|------|-----|
|.\qlik| Qlik1234|

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.0|8 Aug 2017|
