# Qlik Sense Governed Self Service and iPortal 
## Description
This scenario offers a Qlik Sense Server pre-configured iPortal and the Governed Self Service (GSS) modules created by the EA team.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-qs-gss        | 192.168.56.14     | Qlik Sense |

## URLs
| Name | URL | Purpose
|------|-----|---------
|QMC|http://qmi-qs-gss/qmc | QMC
|hub|http://qmi-qs-gss/hub | hub
|iPortal|http://qmi-qs-gss/iportal/hub| hub
|web connectors|http://qmi-qs-gss:5555/web | web connectors

## Users
| Name | Password |
|------|-----|
|.\qlik| Qlik1234|

## Purpose
The purpose of this scenario is to be able to demonstrate the governed self service capabilities in Qlik Sense.

## What is installed
### Software
1. Qlik Sense Server
2. iPortal
3. Governed Self Service
4. GeoAnalytics Base 5.8.1
5. GeoAnalytics Plus 1.7.0
6. Qlik Web Connectors

### Qlik Sense Applications
* Executive Dashboard
* Travel Expense Management
* Sales Management and Customers Analysis
* Customer Experience [Telco]


### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.0|1 Aug 2017|
