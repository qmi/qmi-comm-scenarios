var path = require("path");


var installConfig = {
	thisServer:
	{
		port: 9945,
		hostname: "qmi-qs-gss"
	},
	qsocks:
	{
		host: "qmi-qs-gss"
	},
	engine:
	{
		hostname: "qmi-qs-gss"
	},
	qrs:
	{
		hostname: "qmi-qs-gss"
	}
};


module.exports = installConfig;