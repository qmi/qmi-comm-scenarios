# Qlik Sense Session Applications
## Description
Qlik Sense with Session Applications

| Servername        | Server IP         | Purpose |
|-------------------|-------------------|---------|
| qmi-qs-sa       | 192.168.56.10    | Qlik Sense |

## URLs
| Name | URL | Purpose
|------|-----|---------
|QMC|http://qmi-qs-sa/qmc | QMC
|hub|http://qmi-qs-sa/hub | hub
|Session Apps|http://qmi-qs-sa:4000 | mashup

## Users
| Name | Password |
|------|-----|
|.\qlik| Qlik1234|

## Purpose
Session applications are applications that do not persist on disk, instead the are programatically created on request.  This scenario allows you to demonstrate this capability without the knowledge to develop the solution.

## What is installed
### Software
1. Qlik Sense Server

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Manuel Romero|1.0|8 Aug 2017|
