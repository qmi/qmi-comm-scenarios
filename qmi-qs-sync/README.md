# Qlik Sense Synchronised Persistence
## Description
Qlik Sense server configured with the legacy synchronised persistence architecture.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-qs-sync     | 192.168.56.12    | Qlik Sense |

## URLs
| Name | URL | Purpose
|------|-----|---------
|QMC|http://qmi-qs-sync/qmc | QMC
|hub|http://qmi-qs-sync/hub | hub

## Users
| Name | Password |
|------|-----|
|.\qlik| Qlik1234|

## Purpose
This scenario allows for practicing the upgrade process from Synchronised Persistence to Shared Persistence without interupting your normal environment.

## What is installed
### Software
1. Qlik Sense Server

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Clint Carr|1.0|8 Aug 2017|
