# CentOS with Docker and Kubernetes
## Description
CentOS with Docker CE and Kubernetes.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-centos-docker-kubernetes | 192.168.56.66    | CentOS with Docker and Kubernetes |

## Users
| Name | Password |
|------|-----|
|vagrant|vagrant|

## Connection
Please use __vagrant ssh__ to connect to the server

## Purpose
This scenario is a basic installation of CentOS with Docker preinstalled.

## What is installed
### Software
1. CentOS
2. Docker CE
3. Docker-compose
4. Docker-machine
5. Kubernetes

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Marcus Spitzmiller and Nosheen Faruqui|1.0|5 Dec 2017|

### Updated
| Author | Version | Date Published|
|--------|---------|---------------|
|Clint Carr| 1.01  | 05/14/2017    |

