#
#Module:             kubernetes.sh
#Author:             Marcus Spitzmiller
#Modified by:        Clint Carr
#last updated:       05/14/2018
#
#Modification History:
# - Updated the bridge iptables to use "1" default is "0", prevented kubernetes from functioning.
#
#
#Dependencies: 
# - 
#


echo 'Installing Kubernetes'

sudo curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && sudo chmod +x kubectl && sudo mv kubectl /usr/bin/
sudo curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && sudo chmod +x minikube && sudo mv minikube /usr/bin/

 
echo 'Setting Developer Mode'
echo "export MINIKUBE_WANTUPDATENOTIFICATION=false" >> /home/vagrant/.bash_profile
echo "export MINIKUBE_WANTREPORTERRORPROMPT=false" >> /home/vagrant/.bash_profile
echo "export MINIKUBE_HOME=$HOME" >> /home/vagrant/.bash_profile
echo "export CHANGE_MINIKUBE_NONE_USER=true" >> /home/vagrant/.bash_profile
mkdir /home/vagrant/.kube || true
touch /home/vagrant/.kube/config
 
# set the bridge to 1 else Kubernetes will fail
# see https://wiki.libvirt.org/page/Net.bridge.bridge-nf-call_and_sysctl.conf

echo 1 | sudo tee /proc/sys/net/bridge/bridge-nf-call-iptables > /dev/null

sudo sysctl -p

echo 'Starting Kubernetes'
echo "export KUBECONFIG=/home/vagrant/.kube/config" >> /home/vagrant/.bash_profile
sudo -E /usr/bin/minikube start --vm-driver=none
 
# this for loop waits until kubectl can access the api server that Minikube has created
#for i in {1..150}; do # timeout for 5 minutes
#   ./kubectl get po &> /dev/null
#   if [ $? -ne 1 ]; then
#      break
#  fi
#  sleep 2
#done