#
#Module:             docker.sh
#Author:             Marcus Spitzmiller
#Modified by:        Clint Carr
#last updated:       10/11/2017
#
#Modification History:
# - Changed Docker install to use 17.03 as the latest is not supported by Kubernetes
#
#
#Dependencies: 
# - 
#


echo 'Installing Docker'

sudo yum -y install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

sudo yum-config-manager -y \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

# sudo yum -y install docker-ce

sudo yum install -y --setopt=obsoletes=0 \
  docker-ce-17.03.1.ce-1.el7.centos \
  docker-ce-selinux-17.03.1.ce-1.el7.centos

echo 'Starting docker daemon'
sudo systemctl start docker

sudo usermod -aG docker $USER

echo 'Installing docker-compose'
curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

